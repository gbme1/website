---
author: anna_klassen
title: Genetic Engineering. Its Applications and Limitations. 
year: 2020
image: DNA-Wand.jpg
---

Die Diskussion über die Regulation von Gentechnologie reicht genausoweit zurück wie die Entwicklung dieser Methode selbst. Als es US-amerikanischen Wissenschaftler*innen Anfang der 70er Jahre gelang, replikationsfähige rekombinierte DNA von verschiedenen Spezies herzustellen, wurden aus der scientific community selbst Bedenken in Bezug auf die biologische Sicherheit des neu hergestellten biologischen Materials geäußert. Im Zusammenhang mit dem Problem der biologischen Sicherheit veröffentlichten die führenden Molekularbiologen im Juli 1974 den Aufruf zu einem Moratorium für die Forschung mit rekombinanter DNA, das bis zur viel rezipierten und zitierten Konferenz von Asilomar im Februar 1975 gelten sollte. 

Das vom Gottlieb Duttweiler Institut, einem think tank für wirtschafts- und sozialwissenschaftliche Themen, der Schweizer Gesellschaft für Zell- und Molekularbiologie und dem Forum Davos veranstaltete Symposium „Genetic Engineering. Its Applications and Limitations“ fand schon vor Asilomar statt, nämlich im Oktober 1974. Hintergrund des Symposiums war eine unlängst erfolgte Kürzung der Forschungsgelder durch den Schweizer Gesetzgeber, der die Sensibilität von Wissenschaftler\*innen gegenüber der Diskussion über öffentliche Kontrollen der Wissenschaft erhöht hatte. Die Sicherheitsdebatte in den USA war somit nicht der Anlass für die Veranstaltung, spielte aber aufgrund der Frage von Fremd- und Selbstregulation der Lebenswissenschaften eine entscheidende Rolle. Prominente Sprecher waren Paul Berg, Molekularbiologie und Initiator des Moratoriums, der Reproduktionsmediziner Robert Edwards, der die In-vitro-Fertilisation entwickelte, und Friedrich Vogel, ein Pionier der bundesdeutschen humangenetischen Beratung. 

Geprägt war das Symposium von einer Vielzahl an Themen und Positionen. Auffällig war die große Kluft zwischen Auffassungen der neuen Biotechnologien. Die einen plädierten für eine Neuorientierung des Verhältnisses zwischen Wissenschaft, Öffentlichkeit und Politik, die anderen sahen die Freiheit der Forschung bedroht. Der bisher in der Wissenschaftsgeschichte nicht rezipierte Sammelband liefert neue Einsichten in die frühe Geschichte der Debatten um Gentechnologie, in der Diskursdynamiken noch nicht ausgebildet waren.
