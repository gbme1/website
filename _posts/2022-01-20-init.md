---
layout: post
title: Arbeitstreffen am 24.2.2022
images:
  - hospital.jpg
  - Wissenschaftlerin2.jpg
tags: 
  - particpation
---

## "Leben machen"


### Das interdisziplinäre Arbeitstreffen vernetzt Nachwuchsforscher\*innen, die zur Geschichte der Bio- und Medizinethik in Deutschland forschen.

Am 24.02.2022 findet das interdisziplinäres Arbeitstreffen "Leben machen" des Netzwerks zur Erforschung der Geschichte der Bio- und Medizinethik statt (via Zoom).

Bei dem Arbeitstreffen liegt der inhaltliche Fokus auf der Zeit ab den 70er Jahren, als die Lebenswissenschaften als neue Leitwissenschaften wahrgenommen wurden. Mit den neuen Forschungsfeldern Gentechnik und Reproduktionsmedizin entwickelten sich konfliktträchtige Diskurse rund um das Thema der künstlich-technischen Herstellung und Manipulation von Leben, deren Anwendung – so schien es – unmittelbar bevorstand.
Besonders der Überschneidungsbereich der Technologien wurde in Wissenschaft, Politik und Öffentlichkeit rege diskutiert. Die einen taten die Kombination von Gen- und Reproduktionstechnologien als rein imaginär ab, andere sahen bereits erste Schritte von der Zurichtung der Natur hin zur Züchtung von Menschen.

Das interdisziplinäre und abwechslungsreiche Programm wird dieses Spannungsfeld in den Blick nehmen.

Interessierte melden sich bitte an anna.klassen@uni-jena.de oder anna.schmidt@uni-due.de.

Das Netzwerk dient insbesondere der Vernetzung von in dem Bereich forschenden Nachwuchswissenschaftler*innen und Studierenden, die wir hiermit besonders zur Teilnahme ermutigen wollen.

## Programm

### 10:30-10:45 Einführung: Christina Brandt

### Ethik institutionalisieren - Chair: Christina Brandt

10:45-11:15 Matthis Krischel (Düsseldorf) – Die Institutionalisierung von Forschungsethikkommissionen in Deutschland zwischen internationaler Integration und Reflexion der Medizin im Nationalsozialismus

11:15-11:45 Mathias Schütz (München) – Geburtshelferin der Medizinethik?  berlegungen zur Institutionalisierung von Ethik im Zeitalter der Reproduktionsmedizin

11:45-12:00 Kaffeepause

### Menschen normieren - Chair: Anna Maria Schmidt

12:00-12:30 Lukas Alex (Münster) – Vom langen Schatten in ein neues Licht? Dimensionen der Eugenik in der frühen Bundesrepublik.

12:30-13:00 Susanne Doetz (Berlin) – Die Auseinandersetzungen über philosophische und ethische Probleme in der Humangenetik in der DDR

13:00-14:00 Mittagspause

### Entscheidungen verlagern - Chair: Lukas Alex

14:00-14:30 Denise Lehner-Renken (Goettingen) – „Fortschritt oder Frevel?“ Zur Moralisierung des Marktes für Reproduktionsmedizin in der Bundesrepublik (1978-1990)

14:30-15:00 Claudia Roesch (Washington, D.C.) – Kinder, Kinder, Kinder?! Pro Familia und Debatten um Wunschkinder in Zeit der Neuen Reproduktionstechnologien und Abtreibungsreform (1976-1993)

15:00-15:15 Kaffeepause

### Leben zurichten - Chair: Anna Klassen

15:15-15:45 Daniel Falkner (Marburg) – „Living machines“ - Zur Geschichte einer Metapher und ihre ethische Bedeutung

15:45-16:15 Lena Ulbert (Berlin) – A historical perspective on gene drives and the historical epistemology of genetical behavior

16:15-16:45 Philipp Zeltner (Chemnitz) – Ethische Kontroversen, rechtliche Regulierung und technische Evolution der Reproduktionsmedizin. Einsichten aus der Technikgeschichte des  Social Freezing 

16:45-17:00 Kaffeepause

### Forschungsfelder abstecken - Chair: Christina Brandt

17:00-17:30 Birgit Nemec (Berlin) – Reproduktion und Reproduktionsmedizin als Themenfelder von Wissenschafts- und Gesellschaftsgeschichte – aktuelle Tendenzen und Herausforderungen

17:30-17:45 Abschluss

