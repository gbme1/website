---
---

*Das Forschungsnetzwerk dient dem wissenschaftlichen Austausch über die Geschichte der Bio- und Medizinethik in Deutschland. Insbesondere Nachwuchsforscher\*innen soll die Möglichkeit gegeben werden, sich über Themen und Ansätze zu verständigen, Ideen und Ergebnisse zu präsentieren, Veranstaltungen zu planen und zu besuchen, von der Diskussion verwandter Projekte zu profitieren und Publikationen zu teilen.*