---
position: Wissenschaftlicher Mitarbeiter
prefix: Dr.
name: Mathias Schütz
organizer: true
image: Bild_MS.jpg
contact: 
  email: mathias.schuetz@med.uni-muenchen.de
  tel: +49 892180-72779
brief:
---

Mein Interesse an der Geschichte der Medizin- und Bioethik ist teils der Not, teils dem Zufall zuzuschreiben. Einerseits der Not, an einem einst rein medizinhistorischen, mittlerweile fast rein medizinethischen Institut zu arbeiten, an dem die Auseinandersetzung mit dem (Miss-)Verhältnis von Geschichte und Ethik quasi unausweichlich ist und einem Bedürfnis nach Historisierung der Institutionalisierung von Ethik in der Medizin den Weg geebnet hat. Andererseits dem Zufall, in scheinbar unverwandten Forschungsbereichen auf Argumente zu stoßen, die durch den Aufstieg von Ethik in der Medizin verallgemeinert und durchgesetzt worden sind: Etwa die Rechtfertigung der Requirierung von Leichnamen für die Anatomie durch Berufung auf einen „mutmaßlichen Willen“ der Verstorbenen, der dann ein halbes Jahrhundert später eine zentrale Rolle im Patientenverfügungsgesetz erlangt. Im Rahmen des Netzwerks „Geschichte der Medizin- und Bioethik in Deutschland“ freue ich mich auf neue Impulse und Perspektiven, die über meine Fragestellung der wissenschaftlichen und politischen Legitimierung von Ethik in der Medizin – als Diskurs wie als Disziplin – hinausweisen und Verbindungen mit anderen Forschungsprojekten zur Medizin- und Wissenschaftsgeschichte der 1970er und 1980er Jahre herstellen.