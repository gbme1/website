---
position: Wissenschaftliche Mitarbeiterin
prefix: MA
name: Anna Maria Schmidt
organizer: true
image: Bild_AMS.jpg
contact: 
  email: anna.schmidt@uni-due.de
  tel: +49201-183-
brief:
---

Mein Interesse am praktischen Umgang mit ethischen und ökologischen Fragestellungen begleitet mich seit meinem Studium an der WWU Münster. In meiner Masterarbeit beschäftigte ich mich mit der Entstehung bewusster Ernährung in den 1970er und 1980er Jahren und untersuchte, wie ethisch-ökologische Motive praktisch in der Forderung nach alternativen Ernährungskonzepten umgesetzt wurden.

In meinem im DFG-Graduiertenkolleg 1919 „Vorsorge, Voraussicht, Vorhersage. Kontingenzbewältigung durch Zukunftshandeln“ der Universität Duisburg-Essen angesiedelten Dissertationsprojekt beschäftige ich mich mit der gesellschaftlichen Wahrnehmung der Gentechnologie in der Bundesrepublik Deutschland in den 1980er Jahren. In diesem Rahmen habe ich ein tiefgreifendes Interesse an der Erforschung der Geschichte der Bio- beziehungsweise Lebenswissenschaften entwickelt. Dabei interessiere ich mich anknüpfend an meinen Studienschwerpunkt weniger für die traditionellen Akteur\*innen der Wissenschaftsgeschichte, als vielmehr für Akteur\*innen und Debatten, die sich irgendwo zwischen den Grenzen von Wissenschaft, Politik und Gesellschaft verorten lassen. Im Sinne einer politischen Wissensgeschichte untersuche ich am Beispiel der Gentechnologie die Bedeutung von Wissen als politische Ressource im Kampf um die Deutungshoheit in Bezug auf technologiepolitische Entscheidungen, wobei insbesondere ethische Fragen eine zentrale Rolle spielen. Ich freue mich, über die Zusammenarbeit im Netzwerk „Geschichte der Bio- und Medizinethik in Deutschland“ weitere spannende Forschungsprojekte aus dem Bereich kennenzulernen.
