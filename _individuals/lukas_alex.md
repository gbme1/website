---
position: Wissenschaftlicher Mitarbeiter
prefix: M Ed
name: Lukas Alex
organizer: true
image: lukas_alex.jpg
contact: 
  email: lukas.alex@uni-bayreuth.de
  tel: 
brief:
---

Mein Interesse an bioethischen Debatten entwickelte sich parallel zu meinem Dissertationsprojekt zur Wissensgeschichte der Humangenetik in der frühen Bundesrepublik, welches ich seit dem Abschluss meines Studiums der Geschichte und Mathematik in Münster verfolge. In dem Projekt versuche ich die rassenhygienischen Kontinuitäten und Transformationen humangenetischer Forschungspraktiken und Wissensbestände in der Phase fachlicher Reorganisation präzise zu bestimmen. Als Analyseschema dienen hierbei die Leitkategorien humangenetischer Forschung und Beratung: „Bevölkerung“, „Familie“ und „Individuum“. 

Das Projekt möchte nicht nur die Produktion von Vererbungswissen verfolgen, sondern auch die Zirkulation und Anwendung humangenetischen Wissens in privaten und gesellschaftspolitischen Kontexten untersuchen. Bereits für die 1960er Jahren lässt sich eine Intensivierung des Diskurses um die Anwendbarkeit neuer Technologien zeigen, der innerhalb wie außerhalb der Disziplin geführt wurde. Für ein Verständnis der bio- und medizinethischen Debatten hilft es die Wissenspraktiken an der Scharnierstelle zwischen Diktatur und Demokratie zu betrachten, weil hier doch ein wesentlicher Anteil an der Hervorbringung und Reformulierung dieser Problemstellungen zu vermuten ist.

An dieser Stelle schließen die Fragerichtungen des Netzwerks „Geschichte der Bio- und Medizinethik“ an, von dem ich mir eine breitere Historisierung der bio- und medizinethischen Diskurse erhoffe. Ich freue mich auf spannende Projekt zwischen Philosophie, Wissenschaftsgeschichte sowie Geschichte und Ethik der Medizin. Ich möchte mit meinem Projekt dazu beitragen, die Wechselwirkungen zwischen Wissensproduktion, Anwendungswissen und Technologien sowie ethischen Problemstellungen zu historisieren. 
