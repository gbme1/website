---
position: Wissenschaftliche Mitarbeiterin
prefix: MA
name: Anna Klassen
organizer: true
image: Bild_AK.jpg
contact: 
  email: anna.klassen@uni-jena.de
  tel: +490 36419-49514
brief:
---

Ich habe an der Universität Kassel Philosophie und Germanistik studiert (Bachelor) und den Masterstudiengang „Philosophie der Wissensformen“ absolviert. Bereits während dieser Zeit habe ich mich auf die Philosophie und Geschichte der Lebenswissenschaften spezialisiert; so war ich während des gesamten Masterstudiums Mitglied der Forschungsgruppe „Integrative Biophilosophie“ und beschäftigte mich schwerpunktmäßig mit der Geschichte und Methodologie der Verhaltensforschung an Tieren.

Mein Interesse an den Kontexten der Entstehung und Entwicklung wissenschaftlichen Wissens ist auch leitend für mein Dissertationsprojekt, das ich an der Ruhr-Universität Bochum aufnahm und (aufgrund des Wechsels meiner Betreuerin und Projektleiterin)  an der Friedrich-Schiller-Universität zu Ende führe. 

In meiner Forschung beschäftige ich mich mit der Geschichte der Gentechnologie in den 1970er und 1980er Jahren in der Bundesrepublik. Mich interessieren die verschiedenen Positionen aus Politik, Wissenschaft, Wirtschaft und Öffentlichkeit zu diesem Thema. Im Zusammenhang mit dem Netzwerk „Geschichte der Bio- und Medizinethik in Deutschland“ ist auch die Etablierung einer kritischen Wissenschaftsforschung ein spannendes und wichtiges Forschungsgebiet, hat doch die Auseinandersetzung mit dem Thema Gentechnologie zu ihrer institutionellen Herausbildung beigetragen.
